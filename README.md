**Apps running involves several steps:**
    
  1. cd to the root folder (~/UserProfilerProject)
    
  2. run mvn clean package
    
  3. docker-compose up -d
  
All infrastructure will up.

After deploy the DetailService is accessible for example

`curl --request GET 'http://localhost:8082/api/v1/userDetails?id=11111'
`     

