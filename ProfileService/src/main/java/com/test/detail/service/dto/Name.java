package com.test.detail.service.dto;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

@Data
class Name implements Serializable {

    private final String first;

    @JsonCreator
    public Name(@JsonProperty("first") String first) {
        this.first = first;
    }
}
