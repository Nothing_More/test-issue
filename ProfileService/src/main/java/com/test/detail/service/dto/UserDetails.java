package com.test.detail.service.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

@Data
class UserDetails implements Serializable {
    private final Name name;
    private final String email;

    @JsonCreator
    public UserDetails(@JsonProperty("name") Name name,
                       @JsonProperty("email") String email) {
        this.name = name;
        this.email = email;
    }
}
