package com.test.detail.service.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
public class UserInfo implements Serializable {
    private final String phone;
    private final String name;
    private final String email;

    public static UserInfo emptyUserInfo(String phone) {
        return new UserInfo(phone, "", "");
    }

    public UserInfo withPhone(String phone) {
        return new UserInfo(phone, this.name, this.email);
    }
}
