package com.test.detail.service.service;

import com.test.detail.service.dto.UserInfo;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static java.util.concurrent.CompletableFuture.supplyAsync;

@Service
@RequiredArgsConstructor
public class UserServiceClient {

    private final FetchUserInfoService fetchUserInfoService;

    @Value("${profile.response.timeout}")
    private int timeout;

    public List<UserInfo> getInformationAboutUsers(final List<String> phoneNumbers) {
        return phoneNumbers.parallelStream().map(x ->
            supplyAsync(() -> fetchUserInfoService.fetch(x))
                        .completeOnTimeout(
                                UserInfo.emptyUserInfo(x),
                                timeout,
                                TimeUnit.MILLISECONDS)
        ).collect(Collectors.toList()).stream()
                .map(CompletableFuture::join)
                .collect(Collectors.toList());
    }
}
