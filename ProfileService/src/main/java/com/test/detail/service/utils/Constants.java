package com.test.detail.service.utils;

public final class Constants {

    public static final String API_VERSION =  "/api/v1";

    public static final String USER_DETAIL = API_VERSION + "/userDetails";
}
