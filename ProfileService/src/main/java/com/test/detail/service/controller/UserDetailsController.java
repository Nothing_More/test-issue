package com.test.detail.service.controller;

import com.test.detail.service.dto.ListResponse;
import com.test.detail.service.service.UserServiceClient;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.test.detail.service.utils.Constants.USER_DETAIL;

@RestController
@RequiredArgsConstructor
public class UserDetailsController {

    private final UserServiceClient userServiceClient;

    @PostMapping(path = USER_DETAIL)
    public ResponseEntity<ListResponse> fetchUserDetailsData(@RequestBody List<String> phoneNumbers) {
        return ResponseEntity.ok(new ListResponse(userServiceClient.getInformationAboutUsers(phoneNumbers)));
    }
}
