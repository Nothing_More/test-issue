package com.test.detail.service.service;

import com.test.detail.service.dto.RandomUserResponse;
import com.test.detail.service.dto.UserInfo;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@Log4j2
@RequiredArgsConstructor
public class FetchUserInfoService {

    private final RestTemplate restTemplate;

    @Value("${profile.service.url}")
    private String baseUrl;

    public UserInfo fetch(String phone) {
        try {
            log.info("Fetching user info data by phone number {}", phone);
            RandomUserResponse response = restTemplate.getForEntity(String.format(baseUrl, phone), RandomUserResponse.class).getBody();
            return UserInfo.builder().phone(phone).email(response.email()).name(response.name()).build();
        } catch (Exception ex) {
            log.error("User info for phone number {} fetching failure {}", phone, ex);
            return UserInfo.emptyUserInfo(phone);
        }
    }
}
