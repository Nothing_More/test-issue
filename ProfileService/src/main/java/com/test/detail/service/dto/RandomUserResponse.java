package com.test.detail.service.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;


@Data
public class RandomUserResponse implements Serializable {

    private final List<UserDetails> results;

    @JsonCreator
    public RandomUserResponse(@JsonProperty("results") final List<UserDetails> results) {
        this.results = results;
    }

    public String email() {
        return this.results.get(0).getEmail();
    }

    public String name() {
        return this.results.get(0).getName().getFirst();
    }
}


