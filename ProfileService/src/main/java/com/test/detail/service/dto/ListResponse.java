package com.test.detail.service.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ListResponse implements Serializable {

    private final List<UserInfo> results;

    @JsonCreator
    public ListResponse(@JsonProperty("results") final List<UserInfo> results) {
        this.results = results;
    }
}
