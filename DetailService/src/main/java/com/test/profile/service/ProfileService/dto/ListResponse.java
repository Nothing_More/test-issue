package com.test.profile.service.ProfileService.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class ListResponse {

    private final List<UserInfo> results;

    private final Integer total;

    @JsonCreator
    public ListResponse(@JsonProperty("results") final List<UserInfo> results) {
        this.results = results;
        this.total = results.size();
    }

    public static ListResponse empty() {
        return new ListResponse(Collections.emptyList());
    }

    public static ListResponse getDefaults(List<String> phones) {
        return new ListResponse(phones.stream().map(UserInfo::withCtn).collect(Collectors.toList()));
    }

    public static ListResponse of(List<UserInfo> results) {
        return new ListResponse(results);
    }
}
