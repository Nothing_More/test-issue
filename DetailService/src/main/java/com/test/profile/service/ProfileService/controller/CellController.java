package com.test.profile.service.ProfileService.controller;

import com.test.profile.service.ProfileService.dto.ListResponse;
import com.test.profile.service.ProfileService.service.AbonentDetailsAggregatorService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@AllArgsConstructor
public class CellController {

    private final AbonentDetailsAggregatorService abonentDetailsAggregatorService;

    @GetMapping(path = "/api/v1/userDetails")
    public ResponseEntity<ListResponse> getInfoAboutAbonents(@RequestParam(name = "id") @Valid Long id) {
        return ResponseEntity.ok(abonentDetailsAggregatorService.fetchDetail(id));
    }
}
