package com.test.profile.service.ProfileService.service;

import java.util.List;

public interface SessionService {
    List<String> findPhoneNumbersByCellId(final Long cellId);
}

