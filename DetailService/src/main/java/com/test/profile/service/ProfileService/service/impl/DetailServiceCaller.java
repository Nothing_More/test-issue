package com.test.profile.service.ProfileService.service.impl;

import com.test.profile.service.ProfileService.dto.ListResponse;
import com.test.profile.service.ProfileService.dto.UserInfo;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

@Service
@RequiredArgsConstructor
public class DetailServiceCaller {

    @Value("${detail.service.url}")
    private String url;

    @Value("${request.time-limit}")
    private Long timeLimit;

    private final RestTemplate restTemplate;

    public ListResponse receiveDataFromDetailService(final List<String> phoneNumbers) {
        try {
            return restTemplate.postForEntity(url, phoneNumbers, ListResponse.class).getBody();
        } catch (Exception ex) {
            return ListResponse.empty();
        }
    }

    public List<UserInfo> fetchUserInfo(final List<String> list) {
        return CompletableFuture.supplyAsync(() -> this.receiveDataFromDetailService(list))
                .completeOnTimeout(ListResponse.getDefaults(list), timeLimit, TimeUnit.SECONDS)
                .join()
                .getResults();
    }

}

