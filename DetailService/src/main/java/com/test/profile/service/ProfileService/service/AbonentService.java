package com.test.profile.service.ProfileService.service;


import com.test.profile.service.ProfileService.dto.UserInfo;
import com.test.profile.service.ProfileService.model.Abonent;

import java.util.List;

public interface AbonentService {

    Abonent findByPhoneNumber(final String phoneNumber);

    List<Abonent> fecthAbonentInformation(final List<UserInfo> userInfos);
}