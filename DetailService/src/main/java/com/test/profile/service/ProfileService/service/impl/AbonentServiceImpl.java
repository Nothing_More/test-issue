package com.test.profile.service.ProfileService.service.impl;

import com.test.profile.service.ProfileService.dto.UserInfo;
import com.test.profile.service.ProfileService.model.Abonent;
import com.test.profile.service.ProfileService.repository.AbonentRepository;
import com.test.profile.service.ProfileService.service.AbonentService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AbonentServiceImpl implements AbonentService {

    private final AbonentRepository abonentRepository;

    @Value("${request.time-limit}")
    private Long timeLimit;

    @Override
    @Transactional(readOnly = true)
    public Abonent findByPhoneNumber(String phoneNumber) {
        return abonentRepository.findByPhoneNumber(phoneNumber);
    }

    public List<Abonent> fecthAbonentInformation(final List<UserInfo> userInfos) {
        try {
            return userInfos.parallelStream().map(
                    x -> CompletableFuture.supplyAsync(() -> this.findByPhoneNumber(x.getCtn()))
                            .completeOnTimeout(Abonent.of(x.getCtn()), timeLimit, TimeUnit.SECONDS))
                    .map(CompletableFuture::join)
                    .collect(Collectors.toList());
        } catch (Exception ex) {
            return userInfos.stream().map(x -> Abonent.of(x.getCtn())).collect(Collectors.toList());
        }
    }
}
