package com.test.profile.service.ProfileService.repository;

import com.test.profile.service.ProfileService.model.Abonent;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface AbonentRepository extends PagingAndSortingRepository<Abonent, String> {

    Abonent findByPhoneNumber(final String phoneNumber);
}
