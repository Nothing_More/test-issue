package com.test.profile.service.ProfileService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DetailServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(DetailServiceApplication.class, args);
    }
}
