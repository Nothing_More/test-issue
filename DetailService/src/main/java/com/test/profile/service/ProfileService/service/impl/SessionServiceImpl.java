package com.test.profile.service.ProfileService.service.impl;

import com.test.profile.service.ProfileService.repository.SessionRepository;
import com.test.profile.service.ProfileService.service.SessionService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Service
@AllArgsConstructor
public class SessionServiceImpl implements SessionService {

    private final SessionRepository sessionRepository;

    @Override
    @Transactional(readOnly = true)
    public List<String> findPhoneNumbersByCellId(Long cellId) {
        return sessionRepository.findPhoneNumbersByCellId(cellId);
    }
}
