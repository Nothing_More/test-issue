package com.test.profile.service.ProfileService.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class UserInfo implements Serializable {

    private final String ctn;

    private final String name;

    private final String email;

    private final String uuid;

    @JsonCreator
    public UserInfo(
            @JsonProperty("phone")
                    String ctn,
            @JsonProperty("name")
                    String name,
            @JsonProperty("email")
                    String email,
            @JsonProperty("uuid")
                    String uuid) {
        this.ctn = ctn;
        this.name = name;
        this.email = email;
        this.uuid = uuid;
    }

    public UserInfo withUuid(String uuid) {
        return UserInfo.builder()
                .ctn(this.ctn)
                .email(this.email)
                .name(this.name)
                .uuid(uuid)
                .build();
    }

    public static UserInfo withCtn(String ctn) {
        return UserInfo.builder()
                .ctn(ctn)
                .email("")
                .name("")
                .uuid("")
                .build();
    }
}

