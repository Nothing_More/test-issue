package com.test.profile.service.ProfileService.repository;

import com.test.profile.service.ProfileService.model.Session;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SessionRepository extends PagingAndSortingRepository<Session, Long> {

    @Query(value = "SELECT phone_number FROM session as s WHERE s.cell_id=?1", nativeQuery = true)
    List<String> findPhoneNumbersByCellId(final Long cellId);

}
