package com.test.profile.service.ProfileService.service.impl;

import com.test.profile.service.ProfileService.dto.ListResponse;
import com.test.profile.service.ProfileService.dto.UserInfo;
import com.test.profile.service.ProfileService.model.Abonent;
import com.test.profile.service.ProfileService.service.AbonentDetailsAggregatorService;
import com.test.profile.service.ProfileService.service.AbonentService;
import com.test.profile.service.ProfileService.service.SessionService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AbonentDetailsAggregatorServiceImpl implements AbonentDetailsAggregatorService {

    private final DetailServiceCaller detailServiceCaller;

    private final AbonentService abonentService;

    private final SessionService sessionService;


    @Override
    public ListResponse fetchDetail(Long cellId) {

        final List<String> list = sessionService.findPhoneNumbersByCellId(cellId);

        final List<UserInfo> userInfos = detailServiceCaller.fetchUserInfo(list);

        final Map<String, String> abonents = abonentService.fecthAbonentInformation(userInfos).stream().collect(Collectors.toMap(Abonent::getPhoneNumber, Abonent::getUuid));

        return ListResponse.of(userInfos.stream().map(x -> x.withUuid(abonents.get(x.getCtn()))).collect(Collectors.toList()));
    }


}
