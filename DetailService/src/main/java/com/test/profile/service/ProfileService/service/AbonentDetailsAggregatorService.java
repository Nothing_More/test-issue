package com.test.profile.service.ProfileService.service;

import com.test.profile.service.ProfileService.dto.ListResponse;

public interface AbonentDetailsAggregatorService {

        ListResponse fetchDetail(final Long cellId);
}
